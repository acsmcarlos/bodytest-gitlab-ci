*** Settings ***
Documentation    Ações de autorização


*** Keywords ***
Go To Login Page
    Go To    https://bodytest-web-antonio.herokuapp.com
    Wait For Elements State     css=button >> text=Entrar    visible     15

Login With
    [Arguments]    ${email}    ${pass}

    Fill Text    css=input[name=email]       ${email}
    Fill Text    css=input[name=password]    ${pass}
    Click        text=Entrar
