import psycopg2
from logging import info

class DeloreanLibrary(object):
    def connect(self):
        return psycopg2.connect(
            host='ec2-3-231-103-217.compute-1.amazonaws.com',
            database='dbrpo93md6th7s',
            user='ypkbcccnqumdim',
            password='dd56c0553f4b20af57a96fe6459c59b52a73abf660c6ac94ac12807d83eab056'
        )

    def remove_student(self, email):

        query = "delete from students where email = '{}'".format(email)
        
        info(query)

        conn = self.connect()
        cur = conn.cursor()
        cur.execute(query)
        conn.commit()
        conn.close()

    def remove_student_by_name(self, name):
        query = "delete from students where name LIKE '%{}%'".format(name)
        info(query)

        conn = self.connect()
        cur = conn.cursor()
        cur.execute(query)
        conn.commit()
        conn.close()
        

    def insert_student(self, student):

        self.remove_student(student['email'])

        query = ("insert into students (name, email, age, weight, feet_tall, created_at, updated_at)"
                "values ('{}','{}',{},{},{},now(),now());"
                .format(
                    student['name'],
                    student['email'],
                    student['age'],
                    student['weight'],
                    student['feet_tall']
                )
        )
        info(query)

        conn = self.connect()
        cur = conn.cursor()
        cur.execute(query)
        conn.commit()
        conn.close()

        
  