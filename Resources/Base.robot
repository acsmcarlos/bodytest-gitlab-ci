*** Settings ***
Documentation    Tudo começa, aqui, meu arquivo base do projeto de automação

Library    Browser
Library    String
Library    Collections
Library    Libs/DeloreanLibrary.py
Library    OperatingSystem

Resource    Actions/Auth.robot
Resource    Actions/Students.robot
Resource    Actions/Plans.robot

Resource    Actions/Nav.robot
Resource    Actions/Components.robot


*** Keywords ***
Start Browser Session
    New Browser    chromium       headless=True    slowMo=00:00:01
    New Page       about:blank

Start Admin Session
    Start Browser Session
    Go To Login Page
    Login With                  admin@bodytest.com    pwd123
    User Should Be Logged In    Administrador

Clear LS And Take Screenshot
    Take Screenshot
    LocalStorage Clear

Thinking And Take Screenshot
    [Arguments]    ${timeout}

    Sleep              ${timeout}
    Take Screenshot

### Helpers
Get JSON
    [Arguments]    ${file_name}

    ${file}           Get File    ${EXECDIR}/Resources/Fixtures/${file_name}
    ${json_object}    Evaluate    json.loads($file)                             json

    [Return]    ${json_object}