*** Settings ***
Documentation    Suite de testes de remoção de alunos

Resource    ${EXECDIR}/Resources/Base.robot

Test Setup       Start Admin Session
Test Teardown    Take Screenshot


*** Test Cases ***
Cenário: Remover Aluno Cadastrado
    [Tags]    remove
    
    &{student}    Create Dictionary                      
    ...           name=Marilene Candido
    ...           email=marilene@gmail.com
    ...           age=52
    ...           weight=65
    ...           feet_tall=1.65
    ...           message=Aluno removido com sucesso.
    
    Insert Student                ${student}
    Go To Students
    Search Student By Name        ${student.name}
    Request Removal By Email      ${student.email}
    Confirm Removal
    Toaster Text Should Be        ${student.message}
    Student Should Not Visible    ${student.email}
    
    [Teardown]    Thinking And Take Screenshot    2


Cenário: Não Remover Aluno Cadastrado
    [Tags]    desistir
    
    &{student}    Create Dictionary           
    ...           name=Maria Eduarda
    ...           email=mariaedu@gmail.com
    ...           age=35
    ...           weight=60
    ...           feet_tall=1.50
    
    Insert Student               ${student}
    Go To Students
    Search Student By Name       ${student.name}
    Request Removal By Email     ${student.email}
    Cancel Removal
    Student Should Be Visible    ${student.email}
    Remove Student               ${student.email}
