*** Settings ***
Documentation    Suite de testes de cadastro de alunos

Resource    ${EXECDIR}/Resources/Base.robot

Suite Setup      Start Admin Session
Test Teardown    Take Screenshot

*** Test Cases ***
Cenário: Cadastrar Novo aluno
    [Tags]    new

    &{student}    Create Dictionary                        
    ...           name=Antonio Moreira
    ...           email=antonio@gmail.com
    ...           age=42
    ...           weight=78
    ...           feet_tall=1.69
    ...           message=Aluno cadastrado com sucesso.

    Remove Student            ${student.email}
    Go To Students
    Go To Form Student
    New Student               ${student}
    Toaster Text Should Be    ${student.message}

    [Teardown]    Thinking And Take Screenshot    2

Cenário: Não deve permitir email duplicado
    [Tags]    dup

    &{student}    Create Dictionary         
    ...           name=Carlos Rafael
    ...           email=carlos@yahoo.com
    ...           age=15
    ...           weight=75
    ...           feet_tall=1.75

    Insert Student            ${student}
    Go To Students
    Go To Form Student
    New Student               ${student}
    Toaster Text Should Be    Email já existe no sistema.

    [Teardown]    Thinking And Take Screenshot    2

# Cenário: Todos os campos devem ser obrigatórios
#     [Tags]    all

#     @{expected_alerts}    Set Variable              
#     ...                   Nome é obrigatório
#     ...                   O e-mail é obrigatório
#     ...                   idade é obrigatória
#     ...                   o peso é obrigatório
#     ...                   a Altura é obrigatória

#     @{got_alerts}    Create List

#     Go To Students
#     Go To Form Student
#     Submit Student Form

#     FOR    ${index}        IN RANGE     1    6
#         ${span}            Get Required Alerts    ${index}
#         Append To List     ${got_alerts}          ${span}
#     END
    
#     Log     ${expected_alerts}
#     Log     ${got_alerts}
    
#     Lists Should Be Equal     ${expected_alerts}    ${got_alerts}

# Cenário: Validação dos tipos dos campos
#     [Tags]    type
    
#     [Template]            Check Type Field On Student Form
#     ${EMAIL_FIELD}        email
#     ${AGE_FIELD}          number
#     ${WEIGHT_FIELD}       number
#     ${FEET_TALL_FIELD}    number

# Cenário: Menor de 14 anos nao pode fazer cadastro
#     [Tags]    menor

#     &{student}    Create Dictionary                        
#     ...           name=Benjamin William
#     ...           email=ben@gmail.com
#     ...           age=3
#     ...           weight=20
#     ...           feet_tall=1
#     ...           message=A idade deve ser maior ou igual 14 anos

#     Go To Students
#     Go To Form Student
#     New Student             ${student}
#     Alert Text Should Be    ${student.message}

#     [Teardown]    Thinking And Take Screenshot    2


*** Keywords ***
Check Type Field On Student Form
    [Arguments]    ${element}    ${type}

    Go To Students
    Go To Form Student
    Field Should Be Type   ${element}    ${type}