*** Settings ***
Documentation    Suite de testes de cadastro de alunos

Resource    ${EXECDIR}/Resources/Base.robot

Suite Setup      Start Admin Session
Test Teardown    Take Screenshot

*** Test Cases ***
Cenário: Atualizar Um Aluno Já Cadastrado
    [Tags]    update

    ${fixture}    Get JSON    Students-update.json
    
    ${kamalakhan}    Set Variable    ${fixture['before']}
    ${msmarvel}      Set Variable    ${fixture['after']}
    
    Remove Student By Name    ${kamalakhan['name']}
    Remove Student By Name    ${msmarvel['name']}
    
    Insert Student               ${kamalakhan} 
    Go To Students
    Search Student By Name       ${kamalakhan['name']}
    Go To Student Update Form    ${kamalakhan['email']}
    Update A Student             ${msmarvel} 
    Toaster Text Should Be       Aluno atualizado com sucesso.
    
    [Teardown]     Thinking And Take Screenshot    2

