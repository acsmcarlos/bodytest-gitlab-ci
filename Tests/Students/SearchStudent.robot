*** Settings ***
Documentation    Suite de testes de busca de alunos

Resource    ${EXECDIR}/Resources/Base.robot

Suite Setup      Start Admin Session
Test Teardown    Take Screenshot


*** Test Cases ***
Cenário: Busca Exata
    [Tags]    exata

    &{student}    Create Dictionary           
    ...           name=Ana Clara
    ...           email=anaclara@gmail.com
    ...           age=18
    ...           weight=50
    ...           feet_tall=1.69

    Remove Student By Name            ${student.name}
    Insert Student                    ${student}
    Go To Students
    Search Student By Name            ${student.name}
    Student Name Should Be Visible    ${student.name}
    Total Items Should Be             Total: 1

Cenário: Registro Não Encontrado
    [Tags]    nao_encontrado

    ${name}    Set Variable    Frederico Dico

    Remove Student By Name          ${name}
    Go To Students
    Search Student By Name          ${name}
    Register Should Not Be Found

Cenário: Busca Alunos Por Um Único Termo
    [Tags]    unique
      
    ${fixture}     Get JSON         Students-search.json
    ${students}    Set Variable     ${fixture['students']}

    ${word}     Set Variable     ${fixture['word']}
    ${total}    Set Variable     ${fixture['total']}
   
    Remove Student By Name    ${word}

    FOR    ${item}    IN     @{students}
        Insert Student      ${item}
    END

    Go To Students
    Search Student By Name    ${word}
    
    FOR    ${item}    IN     @{students}
        Student Name Should Be Visible    ${item['name']}
    END
    Total Items Should Be    ${total}

