*** Settings ***
Documentation    Suite de testes de cadastro de planos

Resource    ${EXECDIR}/Resources/Base.robot

Suite Setup      Start Admin Session
Test Teardown    Take Screenshot


*** Test Cases ***
Cenário: Calcular Preço Total do Plano

    &{plan}    Create Dictionary
    ...        title=Plano Teste
    ...        duration=12
    ...        price=19,99
    ...        total=R$ 239,88

    Go To Plans
    Go To Form Plan
    Fill Plan Form           ${plan}
    # Total Plan Should Be     ${plan.total}