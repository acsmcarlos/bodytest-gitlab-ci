*** Settings ***
Documentation    Suite de testes de Login do Adm

Resource    ${EXECDIR}/Resources/Base.robot

Suite Setup      Start Browser Session
Test Teardown    Take Screenshot


*** Test Cases ***
Cenário: Login do Administrador
    [Tags]    happy

    Go To Login Page
    Login With                  admin@bodytest.com    pwd123
    User Should Be Logged In    Administrador

    [Teardown]    Clear LS And Take Screenshot

Cenário: Senha Incorreta
    Go To Login Page
    Login With                admin@bodytest.com               12345
    Toaster Text Should Be    Usuário e/ou senha inválidos.

Cenário: Email Não Cadastrado
    Go To Login Page
    Login With                antonio@gmail.com                123456
    Toaster Text Should Be    Usuário e/ou senha inválidos.

Cenário: Email Incorreto
    Go To Login Page
    Login With              admin&bodytest.com          pwd123
    Alert Text Should Be    Informe um e-mail válido

Cenário: Senha Não Informada
    Go To Login Page
    Login With              admin@bodytest.com       ${EMPTY}
    Alert Text Should Be    A senha é obrigatória

Cenário: Email Não Informado
    Go To Login Page
    Login With              ${EMPTY}                  pwd123
    Alert Text Should Be    O e-mail é obrigatório

Cenário: Email e Senha Não Informados
    Go To Login Page
    Login With              ${EMPTY}                  ${EMPTY}
    Alert Text Should Be    O e-mail é obrigatório
    Alert Text Should Be    A senha é obrigatória


